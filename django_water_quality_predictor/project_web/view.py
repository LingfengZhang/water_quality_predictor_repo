from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators import csrf
import os
import sys
from PIL import Image

os.chdir("../django_water_quality_predictor")
IMAGE_DIR='./static/images'

import pandas as pd

# pip3 install folium
import folium
from folium.plugins import MarkerCluster

# !pip install xarray
import requests
from datetime import datetime
import xarray as xr
# !pip install raster2xyz
import sys
# conda install gdal
from osgeo import gdal
import os
import raster2xyz
from raster2xyz.raster2xyz import Raster2xyz
import pandas as pd
import math
# import pickle5 as pickle
import pickle
from math import radians, cos, sin, asin, sqrt
from datetime import datetime, timedelta
import time
import numpy as np


def Prediction_Pull_given_specific_day(dt_string, typ): # only works for previous two months data
    # Document of variables: https://eccc-msc.github.io/open-data/msc-data/nwp_rdps/readme_rdps-datamart_en/#list-of-variables
    
    rtxyz=Raster2xyz()
    if typ == 'Vosaline':
        path= 'static/Vosaline/'
        if(os.path.exists(path+str(typ)+str(dt_string)+'.csv')):
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        else:
            url = 'http://hpfx.collab.science.gc.ca/'+str(dt_string)+'/WXO-DD/model_riops/netcdf/forecast/polar_stereographic/2d/00/024/'+str(dt_string)+'T00Z_MSC_RIOPS_VOSALINE_DBS-0.5m_PS5km_P024.nc'
            r=requests.get(url,allow_redirects=True)
            open(path+str(typ)+str(dt_string)+'_Initial.nc','wb').write(r.content)
            Ops = gdal.WarpOptions(dstSRS="WGS84", srcSRS="+proj=stere +lat_0=90 +lat_ts=59.99993768695209 +lon_0=-100 +x_0=4245000 +y_0=5295000 +R=6371229 +units=m +no_defs", format="GTiff", outputBounds=[-180,-90,180,90])
            gdal.Warp(path+str(typ)+str(dt_string)+'.tif', path+str(typ)+str(dt_string)+'_Initial.nc', options=Ops)
            rtxyz.translate(path+str(typ)+str(dt_string)+'.tif',path+str(typ)+str(dt_string)+'.csv')
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        
    if typ == 'Precip':
        path= 'static/Accumulated_Precipitation/'
        if(os.path.exists(path+str(typ)+str(dt_string)+'.csv')):
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        else:
            url = 'http://hpfx.collab.science.gc.ca/'+str(dt_string)+'/WXO-DD/model_gem_regional/10km/grib2/00/024/CMC_reg_APCP_SFC_0_ps10km_'+str(dt_string)+'00_P024.grib2'
            r=requests.get(url,allow_redirects=True)
            open(path+str(typ)+str(dt_string)+'_Initial.grib2','wb').write(r.content)
            Ops = gdal.WarpOptions(dstSRS="WGS84", format="GTiff", outputBounds=[-180,-90,180,90])
            gdal.Warp(path+str(typ)+str(dt_string)+'.tif', path+str(typ)+str(dt_string)+'_Initial.grib2', options=Ops)
            rtxyz.translate(path+str(typ)+str(dt_string)+'.tif',path+str(typ)+str(dt_string)+'.csv')
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        
    if typ == 'Temperature':
        path= 'static/Temperature/'
        if(os.path.exists(path+str(typ)+str(dt_string)+'.csv')):
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        else:
            url = 'http://hpfx.collab.science.gc.ca/'+str(dt_string)+'/WXO-DD/model_gem_regional/10km/grib2/00/024/CMC_reg_TMP_TGL_2_ps10km_'+str(dt_string)+'00_P024.grib2'
            r=requests.get(url,allow_redirects=True)
            open(path+str(typ)+str(dt_string)+'_Initial.grib2','wb').write(r.content)
            Ops = gdal.WarpOptions(dstSRS="WGS84", format="GTiff", outputBounds=[-180,-90,180,90])
            gdal.Warp(path+str(typ)+str(dt_string)+'.tif', path+str(typ)+str(dt_string)+'_Initial.grib2', options=Ops)
            rtxyz.translate(path+str(typ)+str(dt_string)+'.tif',path+str(typ)+str(dt_string)+'.csv')
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
    
    df.rename(columns={'x':'Longitude','y':'Latitude','z':typ},inplace=True)                       
    return df

def find_nearest_set_threshold(df, lat, long, threshold=1):
    df_extract_match = df[(df['Longitude']==long) & (df['Latitude']==lat)]
    if(df_extract_match.empty):
        # filter some samples far away
        df = df[(df['Longitude'] >= long-threshold) & (df['Longitude'] <= long+threshold)]
        df = df[(df['Latitude'] >= lat-threshold) & (df['Latitude'] <= lat+threshold)]
        
        if(df.empty):
            return None
        else:
            distances = df.apply(
                lambda row: dist(lat, long, row['Latitude'], row['Longitude']), 
                axis=1)
            closest_Latitude, closest_Longitude = df.loc[distances.idxmin(), 'Latitude'], df.loc[distances.idxmin(), 'Longitude']
            return df[(df['Longitude']==closest_Longitude) & (df['Latitude']==closest_Latitude)]
    else:
        return df_extract_match

def dist(lat1, long1, lat2, long2):
    """
Replicating the same formula as mentioned in Wiki
    """
    # convert decimal degrees to radians 
    lat1, long1, lat2, long2 = map(radians, [lat1, long1, lat2, long2])
    # haversine formula 
    dlon = long2 - long1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    # Radius of earth in kilometers is 6371
    km = 6371* c
    return km

def data_preprocessing(date, imputer, statistic_df):

    day_limit = (datetime.now() - timedelta(days=60-2)).strftime("%Y%m%d")

    given_day = date

    given_day_yesterday = datetime.strptime(given_day, "%Y%m%d") - timedelta(days=1)
    given_day_yesterday = given_day_yesterday.strftime("%Y%m%d")
    given_day_before_yesterday = datetime.strptime(given_day, "%Y%m%d") - timedelta(days=2)
    given_day_before_yesterday = given_day_before_yesterday.strftime("%Y%m%d")

    if(given_day > day_limit):
        df_salinity = Prediction_Pull_given_specific_day(given_day, 'Vosaline')
        df_salinity = df_salinity.drop(df_salinity[df_salinity.Vosaline == 1.000000e+20].index)
        df_temperature = Prediction_Pull_given_specific_day(given_day, 'Temperature')
        df_temperature = df_temperature.drop(df_temperature[df_temperature.Temperature == 9999.0].index)
        
        df_precipitation_given_day = Prediction_Pull_given_specific_day(given_day, 'Precip')
        df_precipitation_given_day = df_precipitation_given_day.drop(df_precipitation_given_day[df_precipitation_given_day.Precip == 9999.0].index)
        
        df_precipitation_given_day_yesterday = Prediction_Pull_given_specific_day(given_day_yesterday, 'Precip')
        df_precipitation_given_day_yesterday = df_precipitation_given_day_yesterday.drop(df_precipitation_given_day_yesterday[df_precipitation_given_day_yesterday.Precip == 9999.0].index)
        
        df_precipitation_given_day_before_yesterday = Prediction_Pull_given_specific_day(given_day_before_yesterday, 'Precip')
        df_precipitation_given_day_before_yesterday = df_precipitation_given_day_before_yesterday.drop(df_precipitation_given_day_before_yesterday[df_precipitation_given_day_before_yesterday.Precip == 9999.0].index)
    
    else:
        raise ValueError("We cannot pull the data online on this date")

    df_whole = pd.read_excel(r'static/data/risk_merged.xlsx')

    df_whole = df_whole.drop(['Unnamed: 0', 'Unnamed: 0.1', 'Unnamed: 0.1.1', 'site_id','FC','logFC','site','Unnamed: 0.1.1','Unnamed: 0.1.1.1'], axis=1)

    # df_whole.columns

    df_precipitation_given_day.columns = ["Longitude", "Latitude", "precip_0_24"]
    df_precipitation_given_day_yesterday.columns = ["Longitude", "Latitude", "precip_24_48"]
    df_precipitation_given_day_before_yesterday.columns = ["Longitude", "Latitude", "precip_48_72"]

    precipitation_merge = pd.merge(df_precipitation_given_day, df_precipitation_given_day_yesterday, how="left", on=["Longitude", "Latitude"])
    precipitation_merge = pd.merge(precipitation_merge, df_precipitation_given_day_before_yesterday, how="left", on=["Longitude", "Latitude"])
    precipitation_merge['precip_0_48'] = precipitation_merge.apply(lambda row: row.precip_0_24 + row.precip_24_48, axis=1)
    precipitation_merge['precip_0_72'] = precipitation_merge['precip_0_48'] + precipitation_merge['precip_48_72']

    seasons_one_hot = pd.DataFrame(columns=['Season_Autumn', 'Season_Spring', 'Season_Summer', 'Season_Winter'])
    seasons_one_hot.loc[0] = [0, 0, 0, 0]
    month = int(date[4:6])
    if(month == 1 or month == 2 or month == 3):
        seasons_one_hot['Season_Winter'].iloc[0] = 1
    elif(month == 4 or month == 5 or month == 6):
        seasons_one_hot['Season_Spring'].iloc[0] = 1
    elif(month == 7 or month == 8 or month == 9):
        seasons_one_hot['Season_Summer'].iloc[0] = 1
    else:
        seasons_one_hot['Season_Autumn'].iloc[0] = 1

    df_predipitation_locations = df_precipitation_given_day[['Longitude', 'Latitude']].drop_duplicates().reset_index(drop=True)
    start = time.process_time()
    collected_location = pd.DataFrame(columns=['Longitude', 'Latitude'])
    tag = 1
    for index in range(len(df_predipitation_locations)):
        latitude = df_predipitation_locations['Latitude'].iloc[index]
        longitude = df_predipitation_locations['Longitude'].iloc[index]

        selected_closest_sample_in_dataset = find_nearest_set_threshold(df_whole, latitude, longitude)
        selected_closest_sample_in_dataset = pd.DataFrame(selected_closest_sample_in_dataset)
        if(selected_closest_sample_in_dataset.empty):
            continue
        else:
            pass

        provinces_one_hot = pd.DataFrame(columns=['Province_BC', 'Province_NB',
        'Province_NL', 'Province_NS', 'Province_PE', 'Province_QC'])
        provinces_one_hot.loc[0] = [0, 0, 0, 0, 0, 0]
        if(selected_closest_sample_in_dataset['province'].iloc[0] == 'BC'):
            provinces_one_hot["Province_BC"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'NB'):
            provinces_one_hot["Province_NB"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'NL'):
            provinces_one_hot["Province_NL"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'NS'):
            provinces_one_hot["Province_NS"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'PE'):
            provinces_one_hot["Province_PE"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'QC'):
            provinces_one_hot["Province_QC"].iloc[0] = 1

    #     print(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'])
        risk_one_hot = pd.DataFrame(columns=['Risk_High Risk/Risque Élevé',
        'Risk_Insufficient Data/Données Insufisantes',
        'Risk_Low Risk/Risque Bas', 'Risk_Moderate Risk/Risque Modéré',
        'Risk_Very Low Risk/Risque Très Bas'])
        risk_one_hot.loc[0] = [0, 0, 0, 0, 0]
        
        if(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] is None):
            pass
        else:
    #         risk_one_hot = pd.get_dummies(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'], prefix='Risk')
            if(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'High Risk/Risque Élevé'):
                risk_one_hot["Risk_High Risk/Risque Élevé"].iloc[0] = 1
            elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Insufficient Data/Données Insufisantes'):
                risk_one_hot["Risk_Insufficient Data/Données Insufisantes"].iloc[0] = 1
            elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Low Risk/Risque Bas'):
                risk_one_hot["Risk_Low Risk/Risque Bas"].iloc[0] = 1
            elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Moderate Risk/Risque Modéré'):
                risk_one_hot["Risk_Moderate Risk/Risque Modéré"].iloc[0] = 1
            elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Very Low Risk/Risque Très Bas'):
                risk_one_hot["Risk_Very Low Risk/Risque Très Bas"].iloc[0] = 1

    #     print(selected_closest_sample_in_dataset.columns)
        
        selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.drop(['Date', 'actionable_level', 'province','Risk Scoring/Évaluation du risque', 'land_landform_feature_0'], axis=1)
        
        df_names = list(selected_closest_sample_in_dataset.columns)
        
        selected_closest_precipitation = find_nearest_set_threshold(precipitation_merge, latitude, longitude)
        
        if(selected_closest_precipitation is None):
            continue
        
        precipitation_24 = selected_closest_precipitation["precip_0_24"].iloc[0]
        precipitation_48 = selected_closest_precipitation["precip_0_48"].iloc[0]
        precipitation_72 = selected_closest_precipitation["precip_0_72"].iloc[0]
        
        selected_salinity = find_nearest_set_threshold(df_salinity, latitude, longitude)
        if(selected_salinity is None):
            pass
        else:
            selected_salinity = selected_salinity["Vosaline"].iloc[0]
        
        selected_temperature = find_nearest_set_threshold(df_temperature, latitude, longitude)
        if(selected_temperature is None):
            pass
        else:
            selected_temperature = selected_temperature["Temperature"].iloc[0]
            
        selected_closest_sample_in_dataset["precip_24"].iloc[0] = precipitation_24
        selected_closest_sample_in_dataset["precip_48"].iloc[0] = precipitation_48
        selected_closest_sample_in_dataset["precip_72"].iloc[0] = precipitation_72
    #     print(selected_closest_sample_in_dataset.columns)
    #     print(selected_closest_sample_in_dataset["Temp_C"])
        selected_closest_sample_in_dataset["Temp_C"].iloc[0] = selected_temperature
        
        selected_closest_sample_in_dataset["Sal_PPT_PPM"].iloc[0] = selected_salinity
        
    #     print(selected_closest_sample_in_dataset)
        selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.reset_index(drop=True)
        selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.loc[[0]]
    #     print(selected_closest_sample_in_dataset.columns)
        selected_closest_sample_in_dataset = imputer.transform(selected_closest_sample_in_dataset)
        selected_closest_sample_in_dataset = pd.DataFrame(selected_closest_sample_in_dataset)
        selected_closest_sample_in_dataset.columns = df_names
        
    #     print(selected_closest_sample_in_dataset.columns)
    #     print(statistic_df)

        for feature_name in selected_closest_sample_in_dataset.columns:
            selected_closest_sample_in_dataset[feature_name] = (selected_closest_sample_in_dataset[feature_name] - 
                                            statistic_df.loc[statistic_df['name'] == feature_name]['mean'].iloc[0])/statistic_df.loc[statistic_df['name'] == feature_name]['std'].iloc[0]
        
        precip_24_preprocessed = selected_closest_sample_in_dataset["precip_24"]
        precip_48_preprocessed = selected_closest_sample_in_dataset["precip_48"]
        precip_72_preprocessed = selected_closest_sample_in_dataset["precip_72"]
        
        selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.drop(['precip_24', 'precip_48', 'precip_72'], axis=1)
        
        preprocessed_results = pd.concat([precip_24_preprocessed, precip_48_preprocessed, precip_72_preprocessed, seasons_one_hot, provinces_one_hot, risk_one_hot, selected_closest_sample_in_dataset], axis=1)
        if(tag == 1):
            preprocessed_results_combine = preprocessed_results
            tag = 0
        else:
            preprocessed_results_combine = pd.concat([preprocessed_results_combine, preprocessed_results])

        collected_location = collected_location.append({'Longitude':longitude, 'Latitude':latitude}, ignore_index=True)

    # print("processing time:", time.process_time() - start)
        
    return preprocessed_results_combine, collected_location

def prediction_pipeline(given_day):
    with open("static/save/statistic_df.pkl","rb") as f:
        statistic_df = pickle.load(f)

    imputer = pickle.load(open('static/save/train_imputer', 'rb'))

    preprocessed_results_combine, collected_location = data_preprocessing(given_day, imputer, statistic_df)

    selected_features = pickle.load(open('static/save/selected_features', 'rb'))
    knn_model = pickle.load(open('static/save/knn_model', 'rb'))

    prediction = knn_model.predict(preprocessed_results_combine[selected_features])

    # print(prediction) # 0 is actionable

    # print(collected_location)

    prediction_dataframe = pd.DataFrame(prediction, columns=['prediction'])

    final_results = pd.concat([collected_location, prediction_dataframe], axis=1)

    return final_results

def map_visualization(given_day):
    final_results = pd.read_csv("static/data/final_results_"+str(given_day)+".csv")

    potential_non_actionable = final_results[final_results['prediction'] == 1]

    # print("potential_non_actionable:", potential_non_actionable)

    if(potential_non_actionable.empty):
        return None
        # print("empty dataframe")

    visualization_results = potential_non_actionable

    mean_latitude = visualization_results.Latitude.mean()
    mean_longitude = visualization_results.Longitude.mean()

    map = folium.Map(location=[mean_latitude, mean_longitude], zoom_start=3, control_scale=True)

    marker_cluster = MarkerCluster().add_to(map)

    for index, location_info in visualization_results.iterrows():
        folium.Marker([location_info["Latitude"], location_info["Longitude"]], popup=location_info["prediction"]).add_to(marker_cluster)

    visualization_html_file_name = "map_visualization.html"

    map.save(os.path.join('project_web/templates',visualization_html_file_name))

    return visualization_html_file_name


# !pip install xarray
import requests
from datetime import datetime
import xarray as xr

# !pip install raster2xyz
import sys
# import gdal
from osgeo import gdal
import os
import raster2xyz
from raster2xyz.raster2xyz import Raster2xyz
import pandas as pd

def single_location_Prediction_Pull_given_specific_day(dt_string, typ): # only works for previous two months data
    # Document of variables: https://eccc-msc.github.io/open-data/msc-data/nwp_rdps/readme_rdps-datamart_en/#list-of-variables
    
    rtxyz=Raster2xyz()
    if typ == 'Vosaline':
        path= 'static/Vosaline/'
        if(os.path.exists(path+str(typ)+str(dt_string)+'.csv')):
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        else:
            url = 'http://hpfx.collab.science.gc.ca/'+str(dt_string)+'/WXO-DD/model_riops/netcdf/forecast/polar_stereographic/2d/00/024/'+str(dt_string)+'T00Z_MSC_RIOPS_VOSALINE_DBS-0.5m_PS5km_P024.nc'
            r=requests.get(url,allow_redirects=True)
            open(path+str(typ)+str(dt_string)+'_Initial.nc','wb').write(r.content)
            Ops = gdal.WarpOptions(dstSRS="WGS84", srcSRS="+proj=stere +lat_0=90 +lat_ts=59.99993768695209 +lon_0=-100 +x_0=4245000 +y_0=5295000 +R=6371229 +units=m +no_defs", format="GTiff", outputBounds=[-180,-90,180,90])
            gdal.Warp(path+str(typ)+str(dt_string)+'.tif', path+str(typ)+str(dt_string)+'_Initial.nc', options=Ops)
            rtxyz.translate(path+str(typ)+str(dt_string)+'.tif',path+str(typ)+str(dt_string)+'.csv')
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        
    if typ == 'Precip':
        path= 'static/Accumulated_Precipitation/'
        if(os.path.exists(path+str(typ)+str(dt_string)+'.csv')):
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        else:
            url = 'http://hpfx.collab.science.gc.ca/'+str(dt_string)+'/WXO-DD/model_gem_regional/10km/grib2/00/024/CMC_reg_APCP_SFC_0_ps10km_'+str(dt_string)+'00_P024.grib2'
            r=requests.get(url,allow_redirects=True)
            open(path+str(typ)+str(dt_string)+'_Initial.grib2','wb').write(r.content)
            Ops = gdal.WarpOptions(dstSRS="WGS84", format="GTiff", outputBounds=[-180,-90,180,90])
            gdal.Warp(path+str(typ)+str(dt_string)+'.tif', path+str(typ)+str(dt_string)+'_Initial.grib2', options=Ops)
            rtxyz.translate(path+str(typ)+str(dt_string)+'.tif',path+str(typ)+str(dt_string)+'.csv')
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        
    if typ == 'Temperature':
        path= 'static/Temperature/'
        if(os.path.exists(path+str(typ)+str(dt_string)+'.csv')):
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
        else:
            url = 'http://hpfx.collab.science.gc.ca/'+str(dt_string)+'/WXO-DD/model_gem_regional/10km/grib2/00/024/CMC_reg_TMP_TGL_2_ps10km_'+str(dt_string)+'00_P024.grib2'
            r=requests.get(url,allow_redirects=True)
            open(path+str(typ)+str(dt_string)+'_Initial.grib2','wb').write(r.content)
            Ops = gdal.WarpOptions(dstSRS="WGS84", format="GTiff", outputBounds=[-180,-90,180,90])
            gdal.Warp(path+str(typ)+str(dt_string)+'.tif', path+str(typ)+str(dt_string)+'_Initial.grib2', options=Ops)
            rtxyz.translate(path+str(typ)+str(dt_string)+'.tif',path+str(typ)+str(dt_string)+'.csv')
            df=pd.read_csv(path+str(typ)+str(dt_string)+'.csv')
    
    df.rename(columns={'x':'Longitude','y':'Latitude','z':typ},inplace=True)                       
    return df

def single_location_find_nearest_set_threshold(df, lat, long, threshold=1):
    df_extract_match = df[(df['Longitude']==long) & (df['Latitude']==lat)]
    if(df_extract_match.empty):
        # filter some samples far away
        df = df[(df['Longitude'] >= long-threshold) & (df['Longitude'] <= long+threshold)]
        df = df[(df['Latitude'] >= lat-threshold) & (df['Latitude'] <= lat+threshold)]
        
        if(df.empty):
            return None
        else:
            distances = df.apply(
                lambda row: dist(lat, long, row['Latitude'], row['Longitude']), 
                axis=1)
            closest_Latitude, closest_Longitude = df.loc[distances.idxmin(), 'Latitude'], df.loc[distances.idxmin(), 'Longitude']
            return df[(df['Longitude']==closest_Longitude) & (df['Latitude']==closest_Latitude)]
    else:
        return df_extract_match

def single_location_data_preprocessing(date, longitude, latitude, imputer, statistic_df):

    day_limit = (datetime.now() - timedelta(days=60-2)).strftime("%Y%m%d")

    given_day = date

    given_day_yesterday = datetime.strptime(given_day, "%Y%m%d") - timedelta(days=1)
    given_day_yesterday = given_day_yesterday.strftime("%Y%m%d")
    given_day_before_yesterday = datetime.strptime(given_day, "%Y%m%d") - timedelta(days=2)
    given_day_before_yesterday = given_day_before_yesterday.strftime("%Y%m%d")

    if(given_day > day_limit):
        df_salinity = single_location_Prediction_Pull_given_specific_day(given_day, 'Vosaline')
        df_salinity = df_salinity.drop(df_salinity[df_salinity.Vosaline == 1.000000e+20].index)
        df_temperature = single_location_Prediction_Pull_given_specific_day(given_day, 'Temperature')
        df_temperature = df_temperature.drop(df_temperature[df_temperature.Temperature == 9999.0].index)
        
        df_precipitation_given_day = single_location_Prediction_Pull_given_specific_day(given_day, 'Precip')
        df_precipitation_given_day = df_precipitation_given_day.drop(df_precipitation_given_day[df_precipitation_given_day.Precip == 9999.0].index)
        
        df_precipitation_given_day_yesterday = single_location_Prediction_Pull_given_specific_day(given_day_yesterday, 'Precip')
        df_precipitation_given_day_yesterday = df_precipitation_given_day_yesterday.drop(df_precipitation_given_day_yesterday[df_precipitation_given_day_yesterday.Precip == 9999.0].index)
        
        df_precipitation_given_day_before_yesterday = single_location_Prediction_Pull_given_specific_day(given_day_before_yesterday, 'Precip')
        df_precipitation_given_day_before_yesterday = df_precipitation_given_day_before_yesterday.drop(df_precipitation_given_day_before_yesterday[df_precipitation_given_day_before_yesterday.Precip == 9999.0].index)
    
    else:
        raise ValueError("We cannot pull the data online on this date")

    df_whole = pd.read_excel(r'static/data/risk_merged.xlsx')

    df_whole = df_whole.drop(['Unnamed: 0', 'Unnamed: 0.1', 'Unnamed: 0.1.1', 'site_id','FC','logFC','site','Unnamed: 0.1.1','Unnamed: 0.1.1.1'], axis=1)

    # df_whole.columns

    df_precipitation_given_day.columns = ["Longitude", "Latitude", "precip_0_24"]
    df_precipitation_given_day_yesterday.columns = ["Longitude", "Latitude", "precip_24_48"]
    df_precipitation_given_day_before_yesterday.columns = ["Longitude", "Latitude", "precip_48_72"]

    precipitation_merge = pd.merge(df_precipitation_given_day, df_precipitation_given_day_yesterday, how="left", on=["Longitude", "Latitude"])
    precipitation_merge = pd.merge(precipitation_merge, df_precipitation_given_day_before_yesterday, how="left", on=["Longitude", "Latitude"])
    precipitation_merge['precip_0_48'] = precipitation_merge.apply(lambda row: row.precip_0_24 + row.precip_24_48, axis=1)
    precipitation_merge['precip_0_72'] = precipitation_merge['precip_0_48'] + precipitation_merge['precip_48_72']

    seasons_one_hot = pd.DataFrame(columns=['Season_Autumn', 'Season_Spring', 'Season_Summer', 'Season_Winter'])
    seasons_one_hot.loc[0] = [0, 0, 0, 0]
    month = int(date[4:6])
    if(month == 1 or month == 2 or month == 3):
        seasons_one_hot['Season_Winter'].iloc[0] = 1
    elif(month == 4 or month == 5 or month == 6):
        seasons_one_hot['Season_Spring'].iloc[0] = 1
    elif(month == 7 or month == 8 or month == 9):
        seasons_one_hot['Season_Summer'].iloc[0] = 1
    else:
        seasons_one_hot['Season_Autumn'].iloc[0] = 1
        
    selected_closest_sample_in_dataset = single_location_find_nearest_set_threshold(df_whole, latitude, longitude)
    selected_closest_sample_in_dataset = pd.DataFrame(selected_closest_sample_in_dataset)
    if(selected_closest_sample_in_dataset.empty):
        error_information = "We cannot get the geo-spatial data based on this location"
        return None, error_information
    
#     print(selected_closest_sample_in_dataset.columns)
#     print(selected_closest_sample_in_dataset.iloc[0])
#     print(selected_closest_sample_in_dataset["province"])
#     provinces_one_hot = pd.get_dummies(selected_closest_sample_in_dataset['province'], prefix='Province')
    provinces_one_hot = pd.DataFrame(columns=['Province_BC', 'Province_NB',
       'Province_NL', 'Province_NS', 'Province_PE', 'Province_QC'])
    provinces_one_hot.loc[0] = [0, 0, 0, 0, 0, 0]
    if(selected_closest_sample_in_dataset['province'].iloc[0] == 'BC'):
        provinces_one_hot["Province_BC"].iloc[0] = 1
    elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'NB'):
        provinces_one_hot["Province_NB"].iloc[0] = 1
    elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'NL'):
        provinces_one_hot["Province_NL"].iloc[0] = 1
    elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'NS'):
        provinces_one_hot["Province_NS"].iloc[0] = 1
    elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'PE'):
        provinces_one_hot["Province_PE"].iloc[0] = 1
    elif(selected_closest_sample_in_dataset['province'].iloc[0] == 'QC'):
        provinces_one_hot["Province_QC"].iloc[0] = 1

#     print(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'])
    risk_one_hot = pd.DataFrame(columns=['Risk_High Risk/Risque Élevé',
       'Risk_Insufficient Data/Données Insufisantes',
       'Risk_Low Risk/Risque Bas', 'Risk_Moderate Risk/Risque Modéré',
       'Risk_Very Low Risk/Risque Très Bas'])
    risk_one_hot.loc[0] = [0, 0, 0, 0, 0]
        
    if(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] is None ):
        pass
    else:
#         risk_one_hot = pd.get_dummies(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'], prefix='Risk')
        if(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'High Risk/Risque Élevé'):
            risk_one_hot["Risk_High Risk/Risque Élevé"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Insufficient Data/Données Insufisantes'):
            risk_one_hot["Risk_Insufficient Data/Données Insufisantes"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Low Risk/Risque Bas'):
            risk_one_hot["Risk_Low Risk/Risque Bas"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Moderate Risk/Risque Modéré'):
            risk_one_hot["Risk_Moderate Risk/Risque Modéré"].iloc[0] = 1
        elif(selected_closest_sample_in_dataset['Risk Scoring/Évaluation du risque'].iloc[0] == 'Very Low Risk/Risque Très Bas'):
            risk_one_hot["Risk_Very Low Risk/Risque Très Bas"].iloc[0] = 1

#     print(selected_closest_sample_in_dataset.columns)
    
    selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.drop(['Date', 'actionable_level', 'province','Risk Scoring/Évaluation du risque', 'land_landform_feature_0'], axis=1)
    
    df_names = list(selected_closest_sample_in_dataset.columns)
    
    selected_closest_precipitation = single_location_find_nearest_set_threshold(precipitation_merge, latitude, longitude)
    
    if(selected_closest_precipitation is None):
        error_information = "We cannot get the precipitation data based on this location on this date"
        return None, error_information
    
    precipitation_24 = selected_closest_precipitation["precip_0_24"].iloc[0]
    precipitation_48 = selected_closest_precipitation["precip_0_48"].iloc[0]
    precipitation_72 = selected_closest_precipitation["precip_0_72"].iloc[0]
    
    selected_salinity = single_location_find_nearest_set_threshold(df_salinity, latitude, longitude)
    if(selected_salinity is None):
        selected_salinity = None
    else:
        selected_salinity = selected_salinity["Vosaline"].iloc[0]
    
    selected_temperature = single_location_find_nearest_set_threshold(df_temperature, latitude, longitude)
    if(selected_temperature is None):
        selected_temperature = None
    else:
        selected_temperature = selected_temperature["Temperature"].iloc[0]
        
    selected_closest_sample_in_dataset["precip_24"].iloc[0] = precipitation_24
    selected_closest_sample_in_dataset["precip_48"].iloc[0] = precipitation_48
    selected_closest_sample_in_dataset["precip_72"].iloc[0] = precipitation_72
#     print(selected_closest_sample_in_dataset.columns)
#     print(selected_closest_sample_in_dataset["Temp_C"])
    selected_closest_sample_in_dataset["Temp_C"].iloc[0] = selected_temperature
    
    selected_closest_sample_in_dataset["Sal_PPT_PPM"].iloc[0] = selected_salinity
    
#     print(selected_closest_sample_in_dataset)
    selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.reset_index(drop=True)
    selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.loc[[0]]
#     print(selected_closest_sample_in_dataset.columns)
    selected_closest_sample_in_dataset = imputer.transform(selected_closest_sample_in_dataset)
    selected_closest_sample_in_dataset = pd.DataFrame(selected_closest_sample_in_dataset)
    selected_closest_sample_in_dataset.columns = df_names
    
#     print(selected_closest_sample_in_dataset.columns)
#     print(statistic_df)

    for feature_name in selected_closest_sample_in_dataset.columns:
        selected_closest_sample_in_dataset[feature_name] = (selected_closest_sample_in_dataset[feature_name] - 
                                        statistic_df.loc[statistic_df['name'] == feature_name]['mean'].iloc[0])/statistic_df.loc[statistic_df['name'] == feature_name]['std'].iloc[0]
    
    precip_24_preprocessed = selected_closest_sample_in_dataset["precip_24"]
    precip_48_preprocessed = selected_closest_sample_in_dataset["precip_48"]
    precip_72_preprocessed = selected_closest_sample_in_dataset["precip_72"]
    
    selected_closest_sample_in_dataset = selected_closest_sample_in_dataset.drop(['precip_24', 'precip_48', 'precip_72'], axis=1)
    
    preprocessed_results = pd.concat([precip_24_preprocessed, precip_48_preprocessed, precip_72_preprocessed, seasons_one_hot, provinces_one_hot, risk_one_hot, selected_closest_sample_in_dataset], axis=1)

    return preprocessed_results, ""

def single_location_prediction_pipeline(given_day, given_longitude, given_latitude):
    with open("static/save/statistic_df.pkl","rb") as f:
        statistic_df = pickle.load(f)

    imputer = pickle.load(open('static/save/train_imputer', 'rb'))

    preprocessed_results, error_information = single_location_data_preprocessing(given_day, given_longitude, given_latitude, imputer, statistic_df) # longitude, latitude

    if(preprocessed_results is None):
        return None, error_information
    selected_features = pickle.load(open('static/save/selected_features', 'rb'))
    knn_model = pickle.load(open('static/save/knn_model', 'rb'))

    prediction = knn_model.predict(preprocessed_results[selected_features])

    print(prediction) # 0 is actionable

    if(prediction[0] == 0):
        prediction_result = "actionable"
        print("actionable")
    else:
        prediction_result = "non actionable"
        print("non actionable")

    return prediction, prediction_result

def upload(request):
    print("current path: ",os.getcwd())
    context = {}
    context['hidden_or_not_information'] = 'none'
    context['hidden_or_not'] = 'none'
    context['information'] = ""
    context['min_date'] = (datetime.now() - timedelta(days=60-2)).strftime("%Y-%m-%d")
    context['max_date'] = datetime.now().strftime("%Y-%m-%d")
    context['form_hidden_or_not'] = 'block'
    context['Date'] = ""
    context['single_location_results'] = 'none'
    context['error_hidden_or_not'] = 'none'

    if request.POST:
        # img_file = request.FILES.get("image")
        
        # context['original_image'] = str(os.path.join('images',img_name))


        given_day = request.POST.get("date")
        context['Date'] = str(given_day)

        given_day = given_day.replace("-", "")
        print("given_day:", str(given_day))

        all_locations_var = request.POST.get("all_locations")

        if(all_locations_var == 'all'):

            if(not os.path.exists("static/data/final_results_"+str(given_day)+".csv")):
                # this function needs a lot of time, maybe several hours
                final_results = prediction_pipeline(given_day)

                # potential_non_actionable = final_results[prediction == 1]

                final_results.to_csv("static/data/final_results_"+str(given_day)+".csv", index=False)

            context['data_download'] = "static/data/final_results_"+str(given_day)+".csv"

            visualization_html_file_name = map_visualization(given_day)

            if(visualization_html_file_name is None):
                context['information'] = 'no area with high fecal coliform in this day'
                context['hidden_or_not_information'] = 'block'
            else:
                context['visualization_map_link'] = visualization_html_file_name
                context['hidden_or_not'] = 'block'

        else:
            longitude = float(request.POST.get("Longitude"))
            latitude = float(request.POST.get("Latitude"))

            prediction, prediction_result = single_location_prediction_pipeline(given_day, longitude, latitude)

            if(prediction is None):
                context['error'] = prediction_result
                context['error_hidden_or_not'] = 'block'
            
            else:

                if(prediction[0] == 0):
                    fecal_coliform_information = "low fecal coliform"
                else:
                    fecal_coliform_information = "high fecal coliform"

                context['FC_information'] = fecal_coliform_information
                context['actionable_or_not'] = prediction_result
                context['single_location_results'] = 'block'

            context['current_longitude'] = str(longitude)
            context['current_latitude'] = str(latitude)
        
        context['form_hidden_or_not'] = 'none'
  
    return render(request, 'upload.html', context)

def map_visualization_view(request):
    context = {}
    return render(request, 'map_visualization.html', context)